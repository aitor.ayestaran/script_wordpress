#!/bin/bash
apt update
apt -y upgrade
apt -y install apache2
apt -y install mariadb-server
#mysql_secure_installation
apt -y install php libapache2-mod-php php-mysql
apt install vsftpd
echo "aitor" > /etc/vsftpd.chroot_list
echo "phpinfo();" > /var/www/html/info.php
mysql -e "CREATE DATABASE pilates"
mysql -e "GRANT ALL ON pilates.* to 'user_pilates'@'localhost' IDENTIFIED BY '1234';"
wget https://es.wordpress.org/latest-es_ES.tar.gz
tar xzf latest-es_ES.tar.gz -C /var/www/html
chown aitor:www-data /var/www/html/wordpress/ -R
chmod g+w /var/www/html/wordpress/ -R
#apt install php-bcmath 
apt -y install php-curl php-dom php-mbstring php-imagick php-zip php-gd
mv /etc/vsftpd.conf /etc/vsftpd.conf.old
mv /home/install_wordpress/vsftpd.conf /etc/vsftpd.conf
mv /etc/php/7.3/apache2/php.ini /etc/php/7.3/apache2/php.ini.old
mv /home/install_wordpress/php.ini /etc/php/7.3/apache2/php.ini
reboot
